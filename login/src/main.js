import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import BootstrapVue from 'bootstrap-vue'
import AuthService from "./../src/ad_auth/authad"

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue);
var userstored = null;


new Vue({
  router,
  render: h => h(App),
  created() {
    if (performance.navigation.type == 1) {
      this.$router.push({name: 'Home'});
    }
  }
}).$mount('#app')


