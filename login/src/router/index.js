import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from "./../ad_auth/authad"
import GraphService from "./../ad_auth/graphad"
import Navigation from "../components/layout/Navigation"

var some_user = null;

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
    ,meta: {
      forgetlogin: true
    }
  },
  {
    path: '/issue',
    name: 'Issue',
    component: () => import('../views/Issue.vue')
    ,meta: {
      beforeIssue: true
    }
  },
  {
    path: '/report',
    name: 'Report',
    component: () => import('../views/Report.vue')
    ,meta: {
      beforeReport: true
    }
    
  }
  ]
});

router.beforeEach((to, from, next) =>{
  const stored_user = new AuthService();
  //la sesion se pierde luego de llamar los datos de ad del usuario logeado 
  some_user = stored_user.getlogged();
  //muestra la informacion de la sesion guardada cada vez que se cambia de ruta
  //sin usar la api todo ok pero cuando se usa para obtener el rol de usuario
  //muestra por consola "null"
  console.log(some_user);
  
  if(to.matched.some(record =>record.meta.beforeIssue)){
    if(!some_user){
      next({
        name: 'Home'
      })
    }
    else{
      next();
    }
  }
  else{
    next();
  };
  if(to.matched.some(record =>record.meta.beforeReport)){
    if(!some_user){
      next({
        name: 'Home'
      })
    }
    else{
      next();
    }
  }
  else{
    next();
  };
  if(to.matched.some(record =>record.meta.forgetlogin)){
    if(some_user){
      stored_user.logout();
      next();
    }
    else{
      next();
    }
  }
  else{
    next();
  }
})

export default router

